package yusak.springdemo;

public interface ICoach {
	public String getDailyWorkout();
	public String getDailyFortune();
}
